# Kilojoule Counter

The kilojoule Counter is a diary app which allows people to enter their daily kilojoule intake in various categories and get a daily total.

The App features (on the home screen):
A "Calculator" for entering your daily intake and exercise and get the daily total.
A list of diary entries which the user can click on to see the entry details.
	-When they view an entry they will be able to scroll to the next and previous entry, or edit the current one.
The average daily intake, averaged over all the entered days.


Calculator:
----------
Every Diary entry will have three intake categories.
	- Breakfast
	- Lunch
	- Dinner

Every Diary entry will have three outtake categories.
	- Three exercise types.
	
The entry will show the total intake, total outtake and net total.

There will be a save entry button and exit home without saving button.

When the save button is clicked they will be able to choose a particular date.


Viewing a diary entry:
---------------------
(When someone taps on a diary entry in the list.)

-Display the entered totals for each category.
-NKI for the day
-Button to navigate next
-Button to navigate previous
-Button for calculator

Home Screen
-----------
Calculator Launcher (at bottom)
Daily average (over all days.)
List of entries

Extras:
-------
The app will also support Portuguese. (Using google transalte.)
When the app is opened it will open to the last screen, even if this was mid entry.
It must support rotating the screen.

