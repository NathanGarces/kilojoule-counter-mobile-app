The following resources were free for use with credits to the author:
-------------------------------------------------------------------

Plate Icon used by Kilojoule Intake:
Author: https://www.flaticon.com/authors/pause08

Fire Icon used for kilojoules Used:
Author: https://www.flaticon.com/authors/freepik

Calendar Icon Used By Date Picker/Calendar:
Author: https://www.flaticon.com/authors/smashicons

Calendar Icon Used in The List View:
Author: https://www.flaticon.com/authors/good-ware

Heart Icon Used in the List View
Author: https://www.flaticon.com/authors/good-ware

House Icon used for the home button in the DiaryEntry View Activity
Author: https://www.flaticon.com/authors/freepik

I created the  rest of the resources using Figma.