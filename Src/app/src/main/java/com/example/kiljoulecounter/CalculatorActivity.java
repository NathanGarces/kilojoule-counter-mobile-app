package com.example.kiljoulecounter;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.ContactsContract;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.CalendarView;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

/**
 * Controller class for the CalculatorActivity, used by a user to either create a new entry or edit an existing one.
 *
 * @Author Nathan Garces
 */
public class CalculatorActivity extends AppCompatActivity {

    /* Instance Variables */
    private TextWatcher totalUpdater;
    private EditText breakfast;
    private EditText lunch;
    private EditText dinner;
    private EditText exercise1;
    private EditText exercise2;
    private EditText exercise3;
    private CalendarView calendar;
    private int selectedYear;
    private int selectedMonth;
    private int selectedDay;
    //Used to tell if we are editing an existing DiaryEntry.
    private boolean editing;
    private DiaryEntry entry;
    private int index;
    //Used to check whether to save the app state or not.
    private boolean saveAppState;
    //Check if date is edited after editing an entry
    private boolean dateEditted;
    //Date when editing an existing entry
    private String originalDate;


    /**
     * onCreate method called when a user begins editing or adding a new entry. Checks if user is editing or creating new entry and initiates the fields accordingly.
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calculator);

        selectedYear = 0;
        selectedMonth = 0;
        selectedDay = 0;
        saveAppState = true;


        //Check for extras (if we are editing a DiaryEntry)
        Bundle extras = getIntent().getExtras();
        if(extras != null){
            //Get index for DiaryEntry
            index = extras.getInt("Index");
            editing = true;
            entry = Diary.diary.get(index);
            originalDate = entry.getDateOfEntry();

            //Set date
            selectedYear = Integer.valueOf(originalDate.substring(0, 4));
            selectedMonth = Integer.valueOf(originalDate.substring(5, 7)) - 1;
            selectedDay = Integer.valueOf(originalDate.substring(8, 10));
            //Fill existing values into the fields.
            fillFieldValues();
            dateEditted = false;

        }
        else{
            editing = false;
        }

        //Check if we are coming from a savedState
        if(DataHandler.savedState.size() > 0){
            //Temp array list
            ArrayList<String> t = DataHandler.savedState;

            //Check if we were editing
            if(t.get(8).equals("true")){
                editing = true;
                entry = Diary.diary.get(Integer.valueOf(t.get(9)));
            }

            //Set intake methods
            ((EditText)findViewById(R.id.breakfastIntake)).setText(t.get(1));
            ((EditText)findViewById(R.id.lunchIntake)).setText(t.get(2));
            ((EditText)findViewById(R.id.dinnerIntake)).setText(t.get(3));

            //Set Used kilojoules
            ((EditText)findViewById(R.id.exercise1)).setText(t.get(4));
            ((EditText)findViewById(R.id.exercise2)).setText(t.get(5));
            ((EditText)findViewById(R.id.exercise3)).setText(t.get(6));

            //Set date
            originalDate = t.get(7);

            selectedYear = Integer.valueOf(originalDate.substring(0, 4));
            selectedMonth = Integer.valueOf(originalDate.substring(5, 7)) - 1;
            selectedDay = Integer.valueOf(originalDate.substring(8, 10));

            setDate(originalDate);
            dateEditted = false;

            DataHandler.clearState();

        }


        //Set listeners on various elements - see code below.
        //Listener for calendar view.
        setDatePickerListener();
        //Listeners for EditText Fields.
        setTextChangeListeners();

        //Sets totals to zero at the start
        updateTotals();
    }

    /**
     * Fills all the EditFields in when a DiaryEntry is being edited.
     */
    public void fillFieldValues(){
        //Set values of the intake EditFields.
        ((EditText)findViewById(R.id.breakfastIntake)).setText(String.valueOf(entry.getBreakfast()));
        ((EditText)findViewById(R.id.lunchIntake)).setText(String.valueOf(entry.getLunch()));
        ((EditText)findViewById(R.id.dinnerIntake)).setText(String.valueOf(entry.getDinner()));

        //Set values of the used EditFields.
        ((EditText)findViewById(R.id.exercise1)).setText(String.valueOf(entry.getExercise1()));
        ((EditText)findViewById(R.id.exercise2)).setText(String.valueOf(entry.getExercise2()));
        ((EditText)findViewById(R.id.exercise3)).setText(String.valueOf(entry.getExercise3()));


        //Set the date
        String dateString = entry.getDateOfEntry();
        setDate(dateString);

    }

    /**
     * Used to set the date.
     * @param dateString String of date in form YYYY/MM/DD.
     */
    public void setDate(String dateString){
        //Set the date
        try {
            CalendarView calendar = (CalendarView) findViewById(R.id.datePicker);
            //Create date object.
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
            Date date = dateFormat.parse(dateString);
            //Set date equal to long value.
            calendar.setDate(date.getTime());
        }
        catch(Exception e){
            e.printStackTrace();
        }
    }


    /**
     * Used to set the date pick listener for the calendar.
     */
    public void setDatePickerListener() {
        calendar = (CalendarView) findViewById(R.id.datePicker);
        calendar.setOnDateChangeListener(new CalendarView.OnDateChangeListener() {
            @Override
            public void onSelectedDayChange(CalendarView view, int year, int month, int dayOfMonth) {
                selectedYear = year;
                selectedMonth = month;
                selectedDay = dayOfMonth;

                if(editing){
                    dateEditted = true;
                }
            }
        });
    }

    /**
     * Used to set the textChange listener for all the EditFields when the activity is started.
     */
    public void setTextChangeListeners(){
    //Used to update totals when a field is potentially edited.

    breakfast = (EditText)findViewById(R.id.breakfastIntake);
    lunch = (EditText)findViewById(R.id.lunchIntake);
    dinner = (EditText)findViewById(R.id.dinnerIntake);

    exercise1 = (EditText)findViewById(R.id.exercise1);
    exercise2 = (EditText)findViewById(R.id.exercise2);
    exercise3 = (EditText)findViewById(R.id.exercise3);

    totalUpdater = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            updateTotals();
        }
    };

        breakfast.addTextChangedListener(totalUpdater);
        lunch.addTextChangedListener(totalUpdater);
        dinner.addTextChangedListener(totalUpdater);

        exercise1.addTextChangedListener(totalUpdater);
        exercise2.addTextChangedListener(totalUpdater);
        exercise3.addTextChangedListener(totalUpdater);
        //<--------------------------------------------------------------------->
    }

    /**
     * Used to update the textviews which display the totals.
     */
    public void updateTotals(){

        //<!---------------------Code for intake values--------------------->
        int breakfastKilojoules = integerForm(breakfast.getText().toString());
        int lunchKilojoules = integerForm(lunch.getText().toString());
        int dinnerKilojoules = integerForm(dinner.getText().toString());


        int totalFood = breakfastKilojoules + lunchKilojoules + dinnerKilojoules;
        TextView intakeTotal = (TextView)findViewById(R.id.intakeTotal);
        String temp = getString(R.string.totalIntakeText) + " " + String.valueOf(totalFood) + " " + getString(R.string.kilojoule_abbrev);
        intakeTotal.setText(temp);

        //<!----------------------Code for outtake values--------------------------->
        int exercise1Kilojoules = integerForm(exercise1.getText().toString());
        int exercise2Kilojoules = integerForm(exercise2.getText().toString());
        int exercise3Kilojoules = integerForm(exercise3.getText().toString());


        int totalExercise = exercise1Kilojoules + exercise2Kilojoules + exercise3Kilojoules;
        TextView outtakeTotal = (TextView)findViewById(R.id.outtakeTotal);
        String temp2 = getString(R.string.totalOuttakeText) + " " + String.valueOf(totalExercise) + " " + getString(R.string.kilojoule_abbrev);
        outtakeTotal.setText(temp2);


        //<!----------------------Update net total--------------------------->
        TextView netTotal = (TextView)findViewById(R.id.netTotal);
        String temp3 = getString(R.string.totalNetKilo) + " " + String.valueOf(totalFood - totalExercise) + " " + getString(R.string.kilojoule_abbrev);
        netTotal.setText(temp3);
    }

    /**
     * Used to grab the date when the user doesn't select a new date and uses today's current date.
     */
    public void getDefaultCalendarTimes(){

        Calendar cal = Calendar.getInstance();

        selectedYear = cal.get(Calendar.YEAR);
        selectedMonth = cal.get(Calendar.MONTH);
        selectedDay = cal.get(Calendar.DAY_OF_MONTH);
    }

    /**
     * Onclick method for the save button, used to either add a new diaryentry or update an existing one.
     * @param view
     */
    public void saveButtonClicked(View view){

        //Used if the user doesn't select a date different from the default/current one
        if (selectedYear == 0 && !(editing)){
            getDefaultCalendarTimes();
        }

        //If we are creating a new DiaryEntry.
        if(!(editing)) {
            //Creates diary entry in the array list.
                    entry = new DiaryEntry(integerForm(breakfast.getText().toString()),
                    integerForm(lunch.getText().toString()),
                    integerForm(dinner.getText().toString()),
                    integerForm(exercise1.getText().toString()),
                    integerForm(exercise2.getText().toString()),
                    integerForm(exercise3.getText().toString()),
                    selectedYear,
                    selectedMonth,
                    selectedDay);

            Diary.addEntry(entry);
            //Save entries
            DataHandler.saveDiaryEntries();
            //Announce diary entry added.
            Toast.makeText(this, "Dairy entry has been added!", Toast.LENGTH_SHORT).show();


        }

        //If we are editing a pre-existing DiaryEntry
        else{
            entry.updateDiaryEntry(integerForm(breakfast.getText().toString()),
                    integerForm(lunch.getText().toString()),
                    integerForm(dinner.getText().toString()),
                    integerForm(exercise1.getText().toString()),
                    integerForm(exercise2.getText().toString()),
                    integerForm(exercise3.getText().toString()),
                    selectedYear,
                    selectedMonth,
                    selectedDay);

            //Sort entries again due to possible date change.
            Diary.sortEntries();
            //Save entries
            DataHandler.saveDiaryEntries();
            //Announce diary entry added.
            Toast.makeText(this, "Diary entry has been updated!", Toast.LENGTH_SHORT).show();
        }

        //Get index of diary entry that was added/edited.
        int index = Diary.diary.indexOf(entry);

        //Make sure app state isn't saved.
        saveAppState = false;
        DataHandler.clearState();

        //Take user to display page.
        Intent intent = new Intent(getApplicationContext(), DiaryView.class);
        intent.putExtra("Index", index);
        startActivity(intent);
        finish();
    }

    /**
     * Used for getting the integer form of the EditText's. If the field is empty it returns 0.
     * @param str The amount/value entered into the EditField.
     * @return Returns the integer value of the entered amount. Returns 0 if nothing was entered.
     */
    public int integerForm(String str){
        //Takes in a string, if it's empty returns 0, else returns the value
        if (str.equals("")){
            return 0;
        }
        return Integer.valueOf(str);
    }

    /**
     * Onclick for the cancel button, acts as a "back" button.
     * @param view
     */
    public void cancelButtonClicked(View view){
        //User wants to go back so we don't save the app state.
        saveAppState = false;
        DataHandler.clearState();
        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
        startActivity(intent);
        finish();

    }

    @Override
    public void onBackPressed() {
        //User wants to go back so we don't save the app state.
        saveAppState = false;
        DataHandler.clearState();
        super.onBackPressed();
    }

    /**
     * Checks if int is below 10, if it is returns int with 0 in front of it.
     * @param number Number from 0 onwards.
     */
    public String checkInt(int number){
        if(number < 10 ){
            return "0" + number;
        }
        //else
        return String.valueOf(number);

    }
    @Override
    protected void onPause() {
        if(saveAppState){
            //Used to save the state data.
            ArrayList<String> data = new ArrayList<>();
            data.add("CalculatorActivity");
            //Add kilojoules Intake.
            data.add(breakfast.getText().toString());
            data.add(lunch.getText().toString());
            data.add(dinner.getText().toString());

            //Add kilojoules used.
            data.add(exercise1.getText().toString());
            data.add(exercise2.getText().toString());
            data.add(exercise3.getText().toString());

            if(selectedYear == 0){
                getDefaultCalendarTimes();
            }

            data.add(checkInt(selectedYear) + "/" + checkInt(selectedMonth + 1) + "/" + checkInt(selectedDay));
            data.add(String.valueOf(editing));
            if(editing){
                data.add(String.valueOf(index));
            }
            //Save the state data.
            DataHandler.saveState(data);
        }
        super.onPause();
    }
}
