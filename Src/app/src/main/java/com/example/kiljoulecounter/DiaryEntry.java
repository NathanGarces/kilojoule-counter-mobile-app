package com.example.kiljoulecounter;

/**
 * DiaryEntry will be used to represent the entry of a user. It will have 3 food categories (for kilojoules intake) and
 * 3 exercise categories (for kilojoules used.).
 * A total for intake.
 * A total for kilojoules used.
 * A Net Total (Intake - Used).
 *
 * @Author Nathan Garces
 */
public class DiaryEntry implements Comparable<DiaryEntry>{

    /* Instance Variables */
    private int totalIntake;
    private int totalOuttake;
    private int totalNet;
    //Day of entry
    private int year;
    private int month;
    private int day;

    //Kilojoules intake
    /* Breakfast, Lunch, Dinner */
    private int[] intakeKilojoules;

    //Kilojoules outtake
    /* Exercise1, Exercise2, Exercise3 */
    private int[] usedKilojoules;



    /**
     * Entry constructor, used whenever a user creates a new diary entry.
     */
    public DiaryEntry(int breakfast, int lunch, int dinner, int exercise1, int exercise2, int exercise3, int year, int month, int day){

        //Initialises the arrays.
        intakeKilojoules = new int[3];
        usedKilojoules = new int[3];

        //Assign Values
        intakeKilojoules[0] = breakfast;
        intakeKilojoules[1] = lunch;
        intakeKilojoules[2] = dinner;

        usedKilojoules[0] = exercise1;
        usedKilojoules[1] = exercise2;
        usedKilojoules[2] = exercise3;


        this.year = year;
        //Add one to account for months starting at 0.
        this.month = month + 1;
        this.day = day;

        totalNet = 0;

        //Calculate all the totals, according to entries.
        calculateTotalNet();
    }

    /**
     * Used to calculate the total intake, by adding the values of the 3 food categories.
     */
    public void calculateTotalIntake(){
        if(intakeKilojoules == null){
            totalIntake = 0;
        }
        else{
            totalIntake = intakeKilojoules[0] + intakeKilojoules[1] + intakeKilojoules[2];
        }
    }

    /**
     * Used to calculate the total kj used, by adding the value of the 3 exercise categories.
     */
    public void calculateTotalOuttake(){
        if(usedKilojoules == null){
            totalOuttake = 0;
        }
        else{
            totalOuttake = usedKilojoules[0] + usedKilojoules[1] + usedKilojoules[2];
        }
    }

    /**
     * Used to calculate the NKI. (Intake - Used), uses calculateTotalIntake and calculateTotalOuttake.
     */
    public void calculateTotalNet(){
        calculateTotalIntake();
        calculateTotalOuttake();

        totalNet = totalIntake - totalOuttake;
    }

    /**
     * Used to update a diary entry when the user edits it.
     */
    public void updateDiaryEntry(int breakfast, int lunch, int dinner, int exercise1, int exercise2, int exercise3, int year, int month, int day){

        //Initialises the arrays.
        intakeKilojoules = new int[3];
        usedKilojoules = new int[3];

        //Assign Values
        intakeKilojoules[0] = breakfast;
        intakeKilojoules[1] = lunch;
        intakeKilojoules[2] = dinner;

        usedKilojoules[0] = exercise1;
        usedKilojoules[1] = exercise2;
        usedKilojoules[2] = exercise3;


        this.year = year;
        //Add one to account for months starting at 0.
        this.month = month + 1;
        this.day = day;

        totalNet = 0;

        //Calculate all the totals, according to entries.
        calculateTotalNet();
    }

    //<---------------------------------Get Methods-------------------------------->
    public String getDateOfEntry(){
        String formatedYear = String.valueOf(year);
        String formatedMonth = String.valueOf(month);
        String formatedDay = String.valueOf(day);

        if (month < 10){
            formatedMonth = "0" + month;
        }
        if (day < 10){
            formatedDay = "0" + day;
        }

        return formatedYear + "/" + formatedMonth + "/" + formatedDay;
    }

    public String getTotalNet(){ return String.valueOf(totalNet); }

    public int getTotalIntake(){ return (intakeKilojoules[0] + intakeKilojoules[1] + intakeKilojoules[2]); }


    public boolean equals(DiaryEntry d){
        if ((d.getDateOfEntry().equals(this.getDateOfEntry())) && (d.usedKilojoules.equals(this.usedKilojoules)) && (d.intakeKilojoules.equals(this.intakeKilojoules))){
            return true;
        }
        return false;
    }

    @Override
    public int compareTo(DiaryEntry d){

        //Check if this year is greater
        if(this.year > d.year){
            return -1;
        }
        //If years are the same we compare months and days.
        else if (this.year == d.year ){
            //If this month is later than the other.
            if(this.month > d.month){
                return -1;
            }
            //If the months are the same we compare days.
            else if (this.month == d.month){
                //If this day is later than d.day
                if (this.day > d.day){
                    return -1;
                }
                //If they have the same day.
                else if (this.day == d.day){
                    return 0;
                }
                //If d.day is later than this day.
                else{
                    return 1;
                }
            }
            //If d.month is later than this.month
            else{
                return 1;
            }
        }

        //If d.year is later than this year.
        return 1;
    }

    public int getBreakfast(){ return intakeKilojoules[0]; }

    public int getLunch(){ return intakeKilojoules[1]; }

    public int getDinner(){ return intakeKilojoules[2]; }

    public int getExercise1(){ return usedKilojoules[0]; }

    public int getExercise2(){ return usedKilojoules[1]; }

    public int getExercise3(){ return usedKilojoules[2]; }

    public int getNetTotal() { return totalNet; }

    //<---------------------------------------------------------------------------->



}
