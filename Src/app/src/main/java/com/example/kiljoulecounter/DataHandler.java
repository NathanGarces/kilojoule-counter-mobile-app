package com.example.kiljoulecounter;

import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;

/**
 * The DataHandler will be used to save and load the diary entries. It will save and load data from shared preferences, always using a thread.
 * @Author Nathan Garces
 */
public class DataHandler {
    /* Instance Variables */
    private static SharedPreferences myPreferences;
    private static SharedPreferences.Editor myEditor;
    //Holds state values.
    public static ArrayList<String> savedState;


    //<!------------------------------------Saving Diary Entries---------------------------------------->
    /**
     * saveDiaryEntry uses Gson to save the ArrayList of Diary entries to the SharedPreferences in json format.
     * All data is saved within a thread.
     */
    public static void saveDiaryEntries(){
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                //Get default shared preferences
                myPreferences = PreferenceManager.getDefaultSharedPreferences(MainActivity.getMainContext());

                //Create the editor object.
                myEditor = myPreferences.edit();

                //Save data as a gson format
                Gson gson = new Gson();
                String data = gson.toJson(Diary.diary);

                myEditor.putString("DiaryEntries", data);
                myEditor.apply();
            }
        });
        thread.start();
    }

    /**
     * loadDataEntries is used to load the ArrayList of data entries from SharedPreferences, using Gson.
     * All data is loaded using a thread.
     */
    public static void loadDiaryEntries() {
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {

                //Used to read in the data.
                myPreferences = PreferenceManager.getDefaultSharedPreferences(MainActivity.getMainContext());
                Gson g = new Gson();
                String data = myPreferences.getString("DiaryEntries", "");

                //Used to create the ArrayList and assign it to Diary.diary.
                Type type = new TypeToken<ArrayList<DiaryEntry>>() {
                }.getType();
                Diary.diary = g.fromJson(data, type);

                //Create ArrayList if it isn't found in memory, added step here to avoid creating list view breaking.
                if (Diary.diary == null) {
                    Diary.diary = new ArrayList<DiaryEntry>();
                }

            }
        });
        thread.start();
    }

    /**
     * clearDiaryEntries is used to clear the ArrayList and data which was previously saved in the SharedPreferences.
     */
    public static void clearDiaryEntries(){
        //Get default shared preferences
        myPreferences = PreferenceManager.getDefaultSharedPreferences(MainActivity.getMainContext());

        //Create the editor object.
        myEditor = myPreferences.edit();

        //Clear SharedPreference
        myEditor.putString("DiaryEntries", "");
        myEditor.apply();

        //Clear ArrayList
        Diary.diary = new ArrayList<DiaryEntry>();
    }

    //<!------------------------------------------------------------------------------------------------->

    //<!------------------------------------Saving System State------------------------------------------>

    /**
     * Used to save the applications current state for when the user exits the app.
     * @param stateData Array containing state variables. Starts with activity name (either CalculatorActivity or DiaryView.)
     */
    public static void saveState(final ArrayList<String> stateData){

        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                //Get default shared preferences
                myPreferences = PreferenceManager.getDefaultSharedPreferences(MainActivity.getMainContext());

                //Create the editor object.
                myEditor = myPreferences.edit();

                //Save data as a gson format
                Gson gson = new Gson();
                String data = gson.toJson(stateData);

                myEditor.putString("savedState", data);
                myEditor.apply();
            }
        });
        thread.start();
    }

    /**
     * Used to clear the SharedPrefs where the state is saved, for after the state has been returned.
     */
    public static void clearState(){
        //Set savedState to null
        savedState = new ArrayList<>();

        //Get default shared preferences
        myPreferences = PreferenceManager.getDefaultSharedPreferences(MainActivity.getMainContext());

        //Create the editor object.
        myEditor = myPreferences.edit();

        //Delete data in shared preferences
        myEditor.putString("savedState", "");
        myEditor.apply();
    }

    /**
     * Used to load the state into the savedState array. Gives easy way of referencing the array.
     */
    public static void loadState(){


        //Used to read in the data.
        myPreferences = PreferenceManager.getDefaultSharedPreferences(MainActivity.getMainContext());
        Gson g = new Gson();
        String data = myPreferences.getString("savedState", "");

        //Used to create the ArrayList and assign it to Diary.diary.
        Type type = new TypeToken<ArrayList<String>>() {
        }.getType();
        savedState = g.fromJson(data, type);

        //Create ArrayList if it isn't found in memory, added step here to avoid creating list view breaking.
        if (savedState == null) {
            savedState = new ArrayList<String>();
        }

    }


    //<-------------------------------------------------------------------------------------------------->
}
