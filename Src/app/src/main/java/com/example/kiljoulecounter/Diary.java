package com.example.kiljoulecounter;

import java.util.ArrayList;
import java.util.Collections;

/**
 * The diary class will be used to store all of the diary entries. The entries will be stored in an ArrayList.
 * The ArrayList will be static to allow all classes to access the same information.
 *
 * @Author Nathan Garces
 */
public class Diary {
    /* Instance Variables */
    public static ArrayList<DiaryEntry> diary;


    //<-------------------------------------- METHODS ---------------------------------------->

    /**
     * Used to add a new diary entry to the ArrayList. If the ArrayList is not initialised it creates an empty ArrayList.
     * @param d The diary entry we want to delete.
     */
    public static void addEntry(DiaryEntry d){
        if (diary == null){
            diary = new ArrayList<>();
        }
        diary.add(d);

        sortEntries();
    }

    /**
     * Used to get the average NKI for all diary entries.
     * @return Returns an integer, which is the average NKI for all diary entries. Returns -1 if the ArrayList is null and 0 if it's empty.
     */
    public static int getAverageIntake(){
        if (diary == null){
            diary = new ArrayList<>();
            return -1;
        }
        else if(diary.size() == 0){
            return 0;
        }


        int total = 0;
        DiaryEntry temp;

        for(int i = 0; i < diary.size(); i++){
            temp = diary.get(i);
            total += Integer.valueOf(temp.getTotalNet());
        }

        if(total == 0){
            return 0;
        }
        return total/diary.size();
    }

    /**
     * Used to remove a diary entry from the ArrayList.
     * @param d The diary entry we are deleting.
     */
    public static void removeEntry(DiaryEntry d){

        //If diary is empty or DiaryEntry is not in the list.
        if(diary == null || diary.indexOf(d) == -1){
            return;
        }
        diary.remove(d);
    }

    /**
     * Used to sort the ArrayList of diary entries by date. The sort method uses the DiaryEntry compareTo method, sorting entries from newest to oldest date.
     */
    public static void sortEntries(){
        Collections.sort(diary);
    }

    /**
     * Used to get a diary entry, based on index.
     * @param index Index of entry we are looking for.
     * @return Returns a DiaryEntry, or null if it cannot be found.
     */
    public static DiaryEntry getEntry(int index){
        return diary.get(index);
    }

    /**
     * Returns the size of the ArrayList, aka the number of DiaryEntries.
     * @return An integer, of the number of diary entries. Returns -1 if the ArrayList is empty.
     */
    public static int getSize(){
        if(diary == null){
            return -1;
        }
        return diary.size();
    }


}
