package com.example.kiljoulecounter;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Controller Class for the DiaryView. Used when a user clicks on an entry in the list, or has just created a new DiaryEntry.
 *
 * @Author Nathan Garces
 */
public class DiaryView extends AppCompatActivity {

    /* Instance Variable */
    //Index of diary entry being displayed.
    private int index;
    private DiaryEntry diaryEntry;
    private boolean previous;
    private boolean next;
    //Used to tell whether to save state or not.
    private boolean saveState;

    /**
     * Oncreate method used when the activity is started. Fills all the fields with the relevant information.
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_diary_view);

        //Get intent information
        Bundle extras = getIntent().getExtras();
        saveState = true;

        if(extras != null){
            index = extras.getInt("Index");
            diaryEntry = Diary.getEntry(index);
            //Set-up the activity/views
            fillFieldValues();
        }

        //Check if coming from saved state
        if(DataHandler.savedState.size() > 0){
            index = Integer.valueOf(DataHandler.savedState.get(1));
            diaryEntry = Diary.getEntry(index);
            fillFieldValues();
            DataHandler.clearState();
        }


        //Checks if we can navigate back or forwards
        checkAvailableOptions();
    }


    /**
     * Used to fill all the fields with their corresponding values.
     */
    private void fillFieldValues(){
        //Set the intake fields
        ((EditText)findViewById(R.id.diaryBreakfastAmount)).setText(String.valueOf(diaryEntry.getBreakfast()) + " " + getString(R.string.kilojoule_abbrev));
        ((EditText)findViewById(R.id.diaryLunchAmount)).setText(String.valueOf(diaryEntry.getLunch() + " " + getString(R.string.kilojoule_abbrev)));
        ((EditText)findViewById(R.id.diaryDinnerAmount)).setText(String.valueOf(diaryEntry.getDinner() + " " + getString(R.string.kilojoule_abbrev)));

        //Set the outtake fields
        ((EditText)findViewById(R.id.diaryExercise1Amount)).setText(String.valueOf(diaryEntry.getExercise1()) + " " + getString(R.string.kilojoule_abbrev));
        ((EditText)findViewById(R.id.diaryExercise2Amount)).setText(String.valueOf(diaryEntry.getExercise2()) + " " + getString(R.string.kilojoule_abbrev));
        ((EditText)findViewById(R.id.diaryExercise3Amount)).setText(String.valueOf(diaryEntry.getExercise3()) + " " + getString(R.string.kilojoule_abbrev));

        //Set the NKI
        ((TextView)findViewById(R.id.diaryNetKilojouleTotal)).setText(getString(R.string.totalNetKilo) + " " + String.valueOf(diaryEntry.getNetTotal()) + " " + getString(R.string.kilojoule_abbrev));

        //Set the date
        ((TextView)findViewById(R.id.diaryDateHeading)).setText(String.valueOf(diaryEntry.getDateOfEntry()));
    }

    /**
     * Onclick method for the home button. Returns the user to the MainActivity.
     * @param view
     */
    public void onHomeClicked(View view){
        //Don't save state
        saveState = false;
        DataHandler.clearState();
        finish();
    }

    /**
     * Onclick method for the previous button. Traverses to previous Diary Entry.
     */
    public void onPreviousClicked(View view){
        //Ensure that we can traverse backwards
        if (previous){
            //Don't save state
            saveState = false;
            //Opens new activity for previous item
            Intent intent = new Intent(getApplicationContext(), DiaryView.class);
            intent.putExtra("Index", index - 1);
            startActivity(intent);

            //Closes current activity.
            finish();
        }
    }

    /**
     * Onclick method for the next button. Traverses to next Diary Entry.
     */
    public void onNextClicked(View view){
        //Ensure that we can traverse backwards
        if (next){
            //Dont save state
            saveState = false;
            //Opens new activity for next item
            Intent intent = new Intent(getApplicationContext(), DiaryView.class);
            intent.putExtra("Index", index + 1);
            startActivity(intent);

            //Closes current activity.
            finish();
        }
    }

    /**
     * Onclick method used for the edit button. Opens the Calculator activity with the current values.
     */
    public void onEditClicked(View view){
        //Don't save state
        saveState = false;
        DataHandler.clearState();

        //Open calculator.
        Intent intent = new Intent(getApplicationContext(), CalculatorActivity.class);
        intent.putExtra("Index", index);
        startActivity(intent);

        //Closes current activity.
        finish();
    }


    /**
     * Used to check whether a user can use the prev/next button based on the item index.
     */
    private void checkAvailableOptions(){

        //Check if previous is available
        if(index == 0){
            previous = false;
            Button prev = (Button)findViewById(R.id.diaryButtonPrev);
            prev.setBackground(getDrawable(R.drawable.greyedbutton));
        }
        else{
            previous = true;
        }

        if(index == (Diary.diary.size() - 1)){
            Button next = (Button)findViewById(R.id.diaryButtonNext);
            next.setBackground(getDrawable(R.drawable.greyedbutton));
        }
        else{
            next = true;
        }
    }

    @Override
    public void onBackPressed() {
        saveState = false;
        DataHandler.clearState();
        super.onBackPressed();
    }

    /**
     * Saves the app state when a user exits the app.
     */
    @Override
    protected void onPause() {
        //Save the current state data
        if(saveState) {
            ArrayList<String> data = new ArrayList<>();
            data.add("DiaryView");
            data.add(String.valueOf(index));

            DataHandler.saveState(data);
        }

        super.onPause();
    }
}
