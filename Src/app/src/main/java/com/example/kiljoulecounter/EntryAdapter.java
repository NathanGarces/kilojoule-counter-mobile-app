package com.example.kiljoulecounter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import java.util.ArrayList;

/**
 * EntryAdapter class is used to format each DiaryEntry for the ListView, uses the list_entry.xml for formatting.
 * @Author Nathan Garces
 */
public class EntryAdapter extends ArrayAdapter<DiaryEntry> {

    /* Instance Variables */
    private Context context;
    private ArrayList<DiaryEntry> diaryEntries;
    private String kjAbbreviation;

    /**
     * EntryAdapter constructor, used to make a new adaptor when a list is being created.
     */
    public EntryAdapter(Context context, ArrayList<DiaryEntry> diaryEntries, String kjAbbreviation){
        super(context, R.layout.list_entry, R.id.rowDate, diaryEntries);
        this.diaryEntries = diaryEntries;
        this.context = context;
        this.kjAbbreviation = kjAbbreviation;
    }

    /**
     * Used to set the values on the list_entry.xml for a specific DiaryEntry.
     * @param position
     * @param convertView
     * @param parent
     * @return
     */
    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater)MainActivity.getMainContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View listRow = inflater.inflate(R.layout.list_entry, parent, false);

        //<!--------------------Set Row Element--------------------->
        TextView date = (TextView)listRow.findViewById(R.id.rowDate);
        TextView netKilo = (TextView)listRow.findViewById(R.id.rowNetKilo);
        ImageView calendarIcon = (ImageView)listRow.findViewById(R.id.calendarIcon);

        date.setText(diaryEntries.get(position).getDateOfEntry());
        netKilo.setText(diaryEntries.get(position).getTotalNet() + " " + kjAbbreviation);

        //<-------------------------------------------------------->
        return listRow;
    }
}
