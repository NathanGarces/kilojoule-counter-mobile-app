package com.example.kiljoulecounter;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CalendarView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.List;

/**
 * MainActivity is the controller class for the Main Activity. It's the main activity, where the diary entries are listed. From here a user
 * can view the average nki for everyday, add a new entry or click on an entry and view the detaisl.
 */
public class MainActivity extends AppCompatActivity {

    /* Instance Variables */
    private static Context context;
    private  ListView listView;
    private  EntryAdapter adapter;

    /**
     * onCreate method, which includes the code used to load the data and populate the linked list.
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //DataHandler.clearDiaryEntries();

        context = getApplicationContext();
        listView = (ListView)findViewById(R.id.diaryEntryList);
        adapter = new EntryAdapter(this, Diary.diary, getResources().getString(R.string.kilojoule_abbrev));


        //Load all diary entries, if there are any.
        DataHandler.loadDiaryEntries();

        listView.post(new Runnable() {
            @Override
            public void run() {
                DataHandler.loadDiaryEntries();
                updateListView();
                updateAverage();
            }
        });

        //Check for an existing appState
        DataHandler.loadState();

        if(DataHandler.savedState.size() > 0){
            if(DataHandler.savedState.get(0).equals("CalculatorActivity")) {
                Intent i = new Intent(getApplicationContext(), CalculatorActivity.class);
                startActivity(i);
            }
            else if(DataHandler.savedState.get(0).equals("DiaryView")){
                Intent i = new Intent(getApplicationContext(), DiaryView.class);
                startActivity(i);
            }
        }

    }




    /**
     * Used to get the MainActivity context for the EntryAdapter.
     * @return Returns MainActivty.java application context.
     */
    public static Context getMainContext(){
        return context;
    }

    /**
     * onRestart called every time someone returns to the home screen, from either the calculator or viewing an entry.
     */
    @Override
    protected void onRestart() {
        updateListView();
        updateAverage();
        super.onRestart();
    }

    /**
     * Used to update the list view, when the Main activity is loaded. Uses the EntryAdapter and list_entry.xml to format each entry.
     * Include's the code for the onClick of each list item.
     */
    public void updateListView(){
        listView = (ListView)findViewById(R.id.diaryEntryList);
        adapter = new EntryAdapter(this, Diary.diary, getResources().getString(R.string.kilojoule_abbrev));

        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(getApplicationContext(), DiaryView.class);
                intent.putExtra("Index", position);
                startActivity(intent);
            }
        });
    }

    /**
     * Onclick method for the addentry button, redirects the user to the "calculator".
     * @param view
     */
    public void addEntry(View view){
        Intent i = new Intent(getApplicationContext(), CalculatorActivity.class);
        startActivity(i);
    }

    /**
     * updateAverage is used to update the average NKI over all days on the bottom of the screen.
     */
    public void updateAverage(){
        TextView average = (TextView) findViewById(R.id.averageNKIText);
        String newValue = getString(R.string.average_nki_text) + " " + Diary.getAverageIntake() + " " + getString(R.string.kilojoule_abbrev);
        average.setText(newValue);

    }


}
